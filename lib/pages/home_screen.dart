import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:evibe/app/database_helper.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

const kAndroidUserAgent = 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) wv AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Mobile Safari/537.36';
String isLaunch = "1";

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  void initState() {
    super.initState();
  }

  void _checkRouteAndOpenWebView(BuildContext context) {
    Navigator.popUntil(context, (route) {
      if (route.settings.name != "/home") {
        String selectedUrl = "https://evibe.in" + route.settings.name;
        if (isLaunch == "2") {
          flutterWebViewPlugin.reloadUrl(selectedUrl);
        } else {
          flutterWebViewPlugin.launch(selectedUrl,
              rect: Rect.fromLTWH(0.0, MediaQuery.of(context).padding.top, MediaQuery.of(context).size.width + 2.00, MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top + 2.00),
              userAgent: kAndroidUserAgent,
              scrollBar: false);
        }
        isLaunch = "2";
      } else {
        flutterWebViewPlugin.launch("https://evibe.in",
            rect: Rect.fromLTWH(0.0, MediaQuery.of(context).padding.top, MediaQuery.of(context).size.width + 2.00, MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top + 2.00),
            userAgent: kAndroidUserAgent,
            scrollBar: false);
        isLaunch = "2";
      }
      return true;
    });
  }

  final flutterWebViewPlugin = FlutterWebviewPlugin();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  Future<void> _makeURLCall(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    }
  }

  Future<void> browserRedirect(String url) async {
    flutterWebViewPlugin.stopLoading();
    _makeURLCall(url);
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(statusBarColor: Color.fromRGBO(237, 28, 114, 1)));

    // Main function
    _checkRouteAndOpenWebView(context);

    flutterWebViewPlugin.onStateChanged.listen((viewState) async {
      if (viewState.type == WebViewState.finishLoad) {
        // hide footer app download links if needed
      }

      if (viewState.type == WebViewState.shouldStart) {
        if (viewState.url.startsWith("https://www.instagram.com") ||
            viewState.url.startsWith('https://www.facebook.com') ||
            viewState.url.startsWith('https://m.facebook.com') ||
            viewState.url.startsWith('mailto:') ||
            viewState.url.startsWith('https://www.twitter.com') ||
            viewState.url.startsWith('https://mobile.twitter.com') ||
            viewState.url.startsWith('https://in.pinterest')) {
          setState(() {
            browserRedirect(viewState.url);
            flutterWebViewPlugin.evalJavascript("\$('.loading-screen-gif').css('display', 'none')");
          });
        } else if (viewState.url.startsWith('https://evibe.in/user/logout')) {
          setState(() {
            flutterWebViewPlugin.close();

            var db = new DatabaseHelper();
            db.deleteUsers();

            Navigator.pushNamed(context, '/login');
          });
        }
      }

      if (viewState.type == WebViewState.startLoad) {
        if (viewState.url.startsWith('https://evibe.in/invite/create/download')) {
          setState(() {
            browserRedirect(viewState.url);
            flutterWebViewPlugin.evalJavascript("\$('.loading-screen-gif').css('display', 'none')");
          });
        } else if (viewState.url.startsWith('https://web.whatsapp.com') || viewState.url.startsWith('whatsapp://send') || viewState.url.startsWith('https://apis.whatsapp.com')) {
          setState(() {
            flutterWebViewPlugin.stopLoading();
            flutterWebViewPlugin.goBack();
            _makeURLCall(viewState.url);
          });
        }
      }
    });

    return Scaffold(key: _scaffoldKey);
  }
}
