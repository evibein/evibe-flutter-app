import 'dart:ui';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:evibe/app/auth.dart';
import 'package:evibe/app/database_helper.dart';
import 'package:evibe/app/user.dart';
import 'package:evibe/app/login_screen_presenter.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/services.dart';

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new LoginScreenState();
  }
}

class LoginScreenState extends State<LoginScreen> implements LoginScreenContract, AuthStateListener {
  String _username, _password;

  var loggedIn = false;
  var firebaseAuth = FirebaseAuth.instance;

  bool _isLoading = false;
  bool _obscureTextLogin = true;

  final formKey = new GlobalKey<FormState>();
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  final flutterWebViewPlugin = FlutterWebviewPlugin();
  final FocusNode myFocusNodeEmailLogin = FocusNode();
  final FocusNode myFocusNodePasswordLogin = FocusNode();

  final FocusNode myFocusNodePassword = FocusNode();
  final FocusNode myFocusNodeEmail = FocusNode();
  final FocusNode myFocusNodeName = FocusNode();

  TextEditingController loginEmailController = new TextEditingController();
  TextEditingController loginPasswordController = new TextEditingController();

  bool _obscureTextSignup = true;
  bool _obscureTextSignupConfirm = true;

  TextEditingController signupEmailController = new TextEditingController();
  TextEditingController signupNameController = new TextEditingController();
  TextEditingController signupPasswordController = new TextEditingController();
  TextEditingController signupConfirmPasswordController = new TextEditingController();

  final GoogleSignIn googleSignIn = GoogleSignIn();
  final FirebaseAuth _auth = FirebaseAuth.instance;

  PageController _pageController;

  Color left = Colors.black;
  Color right = Colors.white;

  LoginScreenPresenter _presenter;

  LoginScreenState() {
    _presenter = new LoginScreenPresenter(this);
    var authStateProvider = new AuthStateProvider();
    authStateProvider.subscribe(this);
  }

  @override
  void dispose() {
    myFocusNodePassword.dispose();
    myFocusNodeEmail.dispose();
    myFocusNodeName.dispose();
    _pageController?.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    _pageController = PageController();
  }

  void _submit() {
    final form = formKey.currentState;
    form.save();

    if (_username == null) {
      _showSnackBar("Email Address cannot be empty");
    } else if (_password.length < 6) {
      _showSnackBar("Password must contain at least 6 charecters");
    } else if (form.validate()) {
      setState(() => _isLoading = true);
      form.save();
      _presenter.doLogin(_username, _password);
    }
  }

  void _signUpSubmit() {
    if (signupNameController.text.length == 0) {
      _showSnackBar("Please enter your full name");
    } else if (signupEmailController.text.length == 0) {
      _showSnackBar("Email address cannot be empty");
    } else if (signupPasswordController.text.length < 6) {
      _showSnackBar("Password must contain at least 6 charecters");
    } else if (signupPasswordController.text != signupConfirmPasswordController.text) {
      _showSnackBar("Passwords do not match");
    } else {
      setState(() => _isLoading = true);
      _presenter.doSignUp(signupNameController.text, signupEmailController.text, signupPasswordController.text);
    }
  }

  void _showSnackBar(String text) {
    scaffoldKey.currentState.showSnackBar(new SnackBar(content: new Text(text)));
  }

  @override
  onAuthStateChanged(AuthState state) {}

  @override
  Widget build(BuildContext context) {
    flutterWebViewPlugin.onStateChanged.listen((viewState) async {
      if (viewState.type == WebViewState.shouldStart) {
        if (viewState.url.startsWith("https://evibe.in/?ref=cusLoginTrue")) {
          flutterWebViewPlugin.close();
          setState(() {
            Navigator.pushNamed(context, '/home');
          });
        }
      }
    });

    return new WillPopScope(
        onWillPop: () async => Future.value(false),
        child: new Scaffold(
            appBar: null,
            key: scaffoldKey,
            body: SingleChildScrollView(
              child: SafeArea(
                child: new Container(
                  decoration: new BoxDecoration(
                    gradient: new LinearGradient(
                        colors: [Color(0xFFffffff), Color(0x01ed3e72)], begin: const FractionalOffset(1.6, 0.6), end: const FractionalOffset(1.8, 1.8), stops: [0.0, 1.0], tileMode: TileMode.clamp),
                  ),
                  child: new Center(
                    child: new Container(
                      child: new Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.07),
                            child: new Image(width: 200, height: 70, fit: BoxFit.fill, image: new AssetImage('assets/images/logo_company.png')),
                          ),
                          Expanded(
                            flex: 2,
                            child: PageView(
                              controller: _pageController,
                              onPageChanged: (i) {
                                if (i == 0) {
                                  setState(() {
                                    right = Colors.white;
                                    left = Colors.black;
                                  });
                                } else if (i == 1) {
                                  setState(() {
                                    right = Colors.black;
                                    left = Colors.white;
                                  });
                                }
                              },
                              children: <Widget>[
                                new Container(
                                  child: _buildSignIn(context),
                                ),
                                new Container(
                                  child: _buildSignUp(context),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                      height: MediaQuery.of(context).size.height * 0.65,
                    ),
                  ),
                  height: MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top,
                ),
              ),
            )));
  }

  Widget _buildSignIn(BuildContext context) {
    return Container(
      child: new Column(
        children: <Widget>[
          Stack(
            alignment: Alignment.topCenter,
            overflow: Overflow.visible,
            children: <Widget>[
              Card(
                elevation: 2.0,
                color: Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8.0),
                ),
                child: Container(
                  width: 300.0,
                  height: 185.0,
                  child: Column(
                    children: <Widget>[
                      new Form(
                        key: formKey,
                        child: new Column(
                          children: <Widget>[
                            new Padding(
                              padding: EdgeInsets.only(top: 20.0, bottom: 20.0, left: 25.0, right: 25.0),
                              child: TextFormField(
                                onSaved: (val) => _username = val,
                                focusNode: myFocusNodeEmailLogin,
                                controller: loginEmailController,
                                keyboardType: TextInputType.emailAddress,
                                style: TextStyle(fontFamily: "WorkSansSemiBold", fontSize: 22.0, color: Colors.black),
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  icon: Icon(
                                    Icons.mail,
                                    color: Colors.black,
                                    size: 22.0,
                                  ),
                                  hintText: "Email Address",
                                  hintStyle: TextStyle(fontFamily: "WorkSansSemiBold", fontSize: 17.0),
                                ),
                              ),
                            ),
                            Container(
                              width: 250.0,
                              height: 1.0,
                              color: Colors.grey[400],
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: 20.0, bottom: 20.0, left: 25.0, right: 25.0),
                              child: TextFormField(
                                onSaved: (val) => _password = val,
                                focusNode: myFocusNodePasswordLogin,
                                controller: loginPasswordController,
                                obscureText: _obscureTextLogin,
                                style: TextStyle(fontFamily: "WorkSansSemiBold", fontSize: 22.0, color: Colors.black),
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  icon: Icon(
                                    Icons.lock,
                                    size: 22.0,
                                    color: Colors.black,
                                  ),
                                  hintText: "Password",
                                  hintStyle: TextStyle(fontFamily: "WorkSansSemiBold", fontSize: 17.0),
                                  suffixIcon: GestureDetector(
                                    onTap: _toggleLogin,
                                    child: Icon(
                                      _obscureTextLogin ? Icons.remove_red_eye : Icons.not_interested,
                                      size: 18.0,
                                      color: Colors.black,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              _isLoading
                  ? new CircularProgressIndicator(valueColor: new AlwaysStoppedAnimation<Color>(Color(0xFFed3e72)))
                  : Container(
                      margin: EdgeInsets.only(top: 165.0),
                      decoration: new BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(5.0)),
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                              color: Color(0x55ed3e72),
                              offset: Offset(1.0, 1.0),
                              blurRadius: 20.0,
                            ),
                          ],
                          color: Color(0xFFed3e72)),
                      child: MaterialButton(
                        highlightColor: Colors.transparent,
                        splashColor: Color(0xFFed3e72),
                        //shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5.0))),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 42.0),
                          child: Text(
                            "LOGIN",
                            style: TextStyle(color: Colors.white, fontSize: 25.0, fontFamily: "WorkSansBold"),
                          ),
                        ),
                        onPressed: _submit,
                      ),
                    ),
            ],
          ),
          Padding(
            padding: EdgeInsets.only(top: 10.0),
            child: FlatButton(
                onPressed: _onSignUpButtonPress,
                child: Text(
                  "Create your Account",
                  style: TextStyle(decoration: TextDecoration.underline, color: Colors.grey, fontSize: 16.0, fontFamily: "WorkSansMedium"),
                )),
          ),
          Padding(
            padding: EdgeInsets.only(top: 30.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                    gradient: new LinearGradient(
                        colors: [
                          Colors.white10,
                          Colors.grey,
                        ],
                        begin: const FractionalOffset(0.0, 0.0),
                        end: const FractionalOffset(1.0, 1.0),
                        stops: [0.0, 1.0],
                        tileMode: TileMode.clamp),
                  ),
                  width: 100.0,
                  height: 1.0,
                ),
                Padding(
                  padding: EdgeInsets.only(left: 15.0, right: 15.0),
                  child: Text(
                    "Or",
                    style: TextStyle(color: Colors.grey, fontSize: 16.0, fontFamily: "WorkSansMedium"),
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    gradient: new LinearGradient(
                        colors: [
                          Colors.grey,
                          Colors.white10,
                        ],
                        begin: const FractionalOffset(0.0, 0.0),
                        end: const FractionalOffset(1.0, 1.0),
                        stops: [0.0, 1.0],
                        tileMode: TileMode.clamp),
                  ),
                  width: 100.0,
                  height: 1.0,
                ),
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 25.0, right: 40.0),
                child: GestureDetector(
                  onTap: () => initiateSignIn("FB"),
                  child: Container(
                      padding: const EdgeInsets.all(15.0),
                      decoration: new BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.white,
                      ),
                      child: new Image.asset('assets/icons/fb-icon.png', width: 40, height: 40)),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 25.0),
                child: GestureDetector(
                  onTap: () => _handleGoogleSignIn().whenComplete(() {}),
                  child: Container(
                      padding: const EdgeInsets.all(15.0),
                      decoration: new BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.white,
                      ),
                      child: new Image.asset('assets/icons/google-icon.png', width: 45, height: 45)),
                ),
              ),
            ],
          ),
        ],
      ),
      height: MediaQuery.of(context).size.height,
    );
  }

  Widget _buildSignUp(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Stack(
            alignment: Alignment.topCenter,
            overflow: Overflow.visible,
            children: <Widget>[
              Card(
                elevation: 2.0,
                color: Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8.0),
                ),
                child: Container(
                  width: 300.0,
                  height: 360.0,
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(top: 20.0, bottom: 20.0, left: 25.0, right: 25.0),
                        child: TextField(
                          focusNode: myFocusNodeName,
                          controller: signupNameController,
                          keyboardType: TextInputType.text,
                          textCapitalization: TextCapitalization.words,
                          style: TextStyle(fontFamily: "WorkSansSemiBold", fontSize: 16.0, color: Colors.black),
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            icon: Icon(
                              FontAwesomeIcons.user,
                              color: Colors.black,
                            ),
                            hintText: "Name",
                            hintStyle: TextStyle(fontFamily: "WorkSansSemiBold", fontSize: 16.0),
                          ),
                        ),
                      ),
                      Container(
                        width: 250.0,
                        height: 1.0,
                        color: Colors.grey[400],
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 20.0, bottom: 20.0, left: 25.0, right: 25.0),
                        child: TextField(
                          focusNode: myFocusNodeEmail,
                          controller: signupEmailController,
                          keyboardType: TextInputType.emailAddress,
                          style: TextStyle(fontFamily: "WorkSansSemiBold", fontSize: 16.0, color: Colors.black),
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            icon: Icon(
                              FontAwesomeIcons.envelope,
                              color: Colors.black,
                            ),
                            hintText: "Email Address",
                            hintStyle: TextStyle(fontFamily: "WorkSansSemiBold", fontSize: 16.0),
                          ),
                        ),
                      ),
                      Container(
                        width: 250.0,
                        height: 1.0,
                        color: Colors.grey[400],
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 20.0, bottom: 20.0, left: 25.0, right: 25.0),
                        child: TextField(
                          focusNode: myFocusNodePassword,
                          controller: signupPasswordController,
                          obscureText: _obscureTextSignup,
                          style: TextStyle(fontFamily: "WorkSansSemiBold", fontSize: 16.0, color: Colors.black),
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            icon: Icon(
                              FontAwesomeIcons.lock,
                              color: Colors.black,
                            ),
                            hintText: "Password",
                            hintStyle: TextStyle(fontFamily: "WorkSansSemiBold", fontSize: 16.0),
                            suffixIcon: GestureDetector(
                              onTap: _toggleSignup,
                              child: Icon(
                                _obscureTextSignup ? FontAwesomeIcons.eye : FontAwesomeIcons.eyeSlash,
                                size: 15.0,
                                color: Colors.black,
                              ),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        width: 250.0,
                        height: 1.0,
                        color: Colors.grey[400],
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 20.0, bottom: 20.0, left: 25.0, right: 25.0),
                        child: TextField(
                          controller: signupConfirmPasswordController,
                          obscureText: _obscureTextSignupConfirm,
                          style: TextStyle(fontFamily: "WorkSansSemiBold", fontSize: 16.0, color: Colors.black),
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            icon: Icon(
                              FontAwesomeIcons.lock,
                              color: Colors.black,
                            ),
                            hintText: "Confirmation",
                            hintStyle: TextStyle(fontFamily: "WorkSansSemiBold", fontSize: 16.0),
                            suffixIcon: GestureDetector(
                              onTap: _toggleSignupConfirm,
                              child: Icon(
                                _obscureTextSignupConfirm ? FontAwesomeIcons.eye : FontAwesomeIcons.eyeSlash,
                                size: 15.0,
                                color: Colors.black,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              _isLoading
                  ? new CircularProgressIndicator(valueColor: new AlwaysStoppedAnimation<Color>(Color(0xFFed3e72)))
                  : Container(
                      margin: EdgeInsets.only(top: 340.0),
                      decoration: new BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(5.0)),
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                              color: Color(0x55ed3e72),
                              offset: Offset(1.0, 1.0),
                              blurRadius: 20.0,
                            ),
                          ],
                          color: Color(0xFFed3e72)),
                      child: MaterialButton(
                        highlightColor: Colors.transparent,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 42.0),
                          child: Text(
                            "SIGN UP",
                            style: TextStyle(color: Colors.white, fontSize: 25.0, fontFamily: "WorkSansBold"),
                          ),
                        ),
                        onPressed: _signUpSubmit,
                      ),
                    )
            ],
          ),
          Padding(
            padding: EdgeInsets.only(top: 10.0),
            child: FlatButton(
                onPressed: _onSignInButtonPress,
                child: Text(
                  "Already have an account? Sign in",
                  style: TextStyle(decoration: TextDecoration.underline, color: Colors.grey, fontSize: 16.0, fontFamily: "WorkSansMedium"),
                )),
          ),
        ],
      ),
      height: MediaQuery.of(context).size.height,
    );
  }

  void _onSignInButtonPress() {
    _pageController.animateToPage(0, duration: Duration(milliseconds: 500), curve: Curves.decelerate);
  }

  void _onSignUpButtonPress() {
    _pageController?.animateToPage(1, duration: Duration(milliseconds: 500), curve: Curves.decelerate);
  }

  // Loading animation after login button click
  void _toggleLogin() {
    setState(() {
      _obscureTextLogin = !_obscureTextLogin;
    });
  }

  void _toggleSignup() {
    setState(() {
      _obscureTextSignup = !_obscureTextSignup;
    });
  }

  void _toggleSignupConfirm() {
    setState(() {
      _obscureTextSignupConfirm = !_obscureTextSignupConfirm;
    });
  }

  // Rest apis login error handling
  @override
  void onLoginError(String errorTxt) {
    _showSnackBar(errorTxt.replaceFirst("Exception:", ""));
    setState(() => _isLoading = false);
  }

  // Rest apis login success handling
  @override
  void onLoginSuccess(User user) async {
    var db = new DatabaseHelper();
    await db.saveUser(user);
    var authStateProvider = new AuthStateProvider();
    authStateProvider.notify(AuthState.LOGGED_IN);

    flutterWebViewPlugin.launch(
      "https://evibe.in/customer/login/app?uId=" + user.username + "&access-token=" + user.password,
      rect: Rect.fromLTWH(0.0, 1.00, 1.00, 1.00),
    );
  }

  // Initiating FB signin
  void initiateSignIn(String type) {
    setState(() => _isLoading = true);
    _handleSignIn(type).then((result) {
      if (result == 1) {
        setState(() {
          loggedIn = true;
        });
      }
    });
  }

  // Handling FB signin after logging in
  Future<FacebookLoginResult> _handleFBSignIn() async {
    FacebookLogin facebookLogin = FacebookLogin();
    FacebookLoginResult facebookLoginResult = await facebookLogin.logInWithReadPermissions(['email']);
    switch (facebookLoginResult.status) {
      case FacebookLoginStatus.cancelledByUser:
        print("Cancelled");
        break;
      case FacebookLoginStatus.error:
        print("error");
        break;
      case FacebookLoginStatus.loggedIn:
        var graphResponse = await http.get('https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email,picture.height(200)&access_token=${facebookLoginResult.accessToken.token}');
        var profile = json.decode(graphResponse.body);

        var db = new DatabaseHelper();
        User user = new User.map({"username": profile['email'], "password": profile['id']});
        await db.saveUser(user);

        var authStateProvider = new AuthStateProvider();
        authStateProvider.notify(AuthState.LOGGED_IN);

        flutterWebViewPlugin.launch(
          "https://evibe.in/customer/login/app/facebook?uId=" + profile['id'] + "&email=" + profile['email'] + "&name=" + profile['name'] + "&url=" + profile['picture']['data']['url'],
          rect: Rect.fromLTWH(0.0, 1.00, 1.00, 1.00),
        );

        break;
    }
    return facebookLoginResult;
  }

  // Handling google signin after logging in
  Future<GoogleSignInAccount> _handleGoogleSignIn() async {
    setState(() => _isLoading = true);

    final GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
    final GoogleSignInAuthentication googleSignInAuthentication = await googleSignInAccount.authentication;

    final AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: googleSignInAuthentication.accessToken,
      idToken: googleSignInAuthentication.idToken,
    );

    final FirebaseUser user = await _auth.signInWithCredential(credential);

    var db = new DatabaseHelper();
    User userData = new User.map({"username": user.email, "password": user.uid});
    await db.saveUser(userData);

    var authStateProvider = new AuthStateProvider();
    authStateProvider.notify(AuthState.LOGGED_IN);

    flutterWebViewPlugin.launch(
      "https://evibe.in/customer/login/app/google?uId=" + user.uid + "&email=" + user.email + "&url=" + user.photoUrl + "&name=" + user.displayName,
      rect: Rect.fromLTWH(0.0, 1.00, 1.00, 1.00),
    );

    return googleSignInAccount;
  }

  Future<int> _handleSignIn(String type) async {
    switch (type) {
      case "FB":
        FacebookLoginResult facebookLoginResult = await _handleFBSignIn();
        if (facebookLoginResult.status == FacebookLoginStatus.loggedIn) {
          return 1;
        } else
          return 0;
        break;
    }
    return 0;
  }
}
