import 'dart:async';
import 'package:flutter/material.dart';
import 'package:evibe/pages/home_screen.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:evibe/pages/login_screen.dart';
import 'package:evibe/app/database_helper.dart';

enum AuthState { LOGGED_IN, LOGGED_OUT }

void main() => runApp(new MyApp());

// Main function
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      theme: ThemeData(),
      home: SplashScreen(),
      onGenerateRoute: _getRoute,
    );
  }
}

// Dynamic route declaration
Route _getRoute(RouteSettings settings) {
  switch (settings.name) {
    case '/home':
      return _buildRoute(settings, MyHomePage());
    case '/login':
      return _buildRoute(settings, LoginScreen());
    default:
      return _buildRoute(settings, MyHomePage());
  }
}

// Dynamic route declaration
MaterialPageRoute _buildRoute(RouteSettings settings, Widget builder) {
  return new MaterialPageRoute(
    settings: settings,
    builder: (BuildContext context) => builder,
  );
}

// Initialisation of firebase
final FirebaseMessaging _fireBaseMessaging = new FirebaseMessaging();

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  void initState() {
    super.initState();
    redirectAfterSplashScreen();
  }

  void redirectAfterSplashScreen() async {
    bool isLoggedIn = false;
    String isNotificationLaunch = "1";

    var db = new DatabaseHelper();
    var isLoggedInDB = await db.isLoggedIn();
    if (isLoggedInDB) {
      isLoggedIn = true;
    }

    // Configuring firebase triggers
    _fireBaseMessaging.configure(
      onLaunch: (Map<String, dynamic> message) async {
        isNotificationLaunch = "2";
        if (isLoggedIn) {
          final String pageChooser = message['data']['page'];
          Timer(Duration(seconds: 2), () => Navigator.pushNamed(context, pageChooser));
        } else {
          Timer(Duration(seconds: 2), () => Navigator.pushNamed(context, '/login'));
        }
      },
      onResume: (Map<String, dynamic> message) async {
        if (isLoggedIn) {
          final String pageChooser = message['data']['page'];
          Navigator.pushNamed(context, pageChooser);
        }
      },
      onMessage: (Map<String, dynamic> message) async {},
    );

    if (isNotificationLaunch == "1") {
      Timer(Duration(seconds: 2), () {
        if (isLoggedIn) {
          Navigator.pushNamed(context, "/home");
        } else {
          Navigator.pushNamed(context, "/login");
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Stack(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(color: const Color(0xffed3e72)),
            ),
            Center(
              child: Container(
                width: MediaQuery.of(context).size.width * 0.8,
                child: Padding(
                  padding: EdgeInsets.all(0),
                  child: new Container(
                    decoration: new BoxDecoration(
                        image: DecorationImage(
                      image: new AssetImage('assets/images/logo_white.png'),
                    )),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class Item {
  Item({this.itemId});

  final String itemId;
  StreamController<Item> _controller = new StreamController<Item>.broadcast();

  Stream<Item> get onChanged => _controller.stream;
  String _status;

  String get status => _status;

  set status(String value) {
    _status = value;
    _controller.add(this);
  }

  static final Map<String, Route<Null>> routes = <String, Route<Null>>{};
}
